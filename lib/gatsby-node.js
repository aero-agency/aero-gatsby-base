var ExtractTextPlugin = require('extract-text-webpack-plugin');
var OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
var glob = require('glob');
var path = require('path');

// Get all the .less files in the css directory that don't start with an underscore.
var stylesheets = glob.sync(process.cwd() + '/css/*.less', {
  ignore: './css/_*.less'
}).map(function(filename) {
  return path.basename(filename, '.less');
});

module.exports.modifyWebpackConfig = function(config, stage) {
  config.removeLoader('less');

  stylesheets.forEach((page) => {
    config.loader(page + '-css', {
      test: new RegExp('css/' + page + '.less'),
      loaders: ['file?name=css/' + page + '.css', 'extract', 'raw?minimize', 'less']
    });
  });

  if (stage === 'build-css') {
    // The default gatsby webpack includes an ExtractTextPlugin to generate styles.css,
    // but we want to build our own page specific css files, so clear out the plugins.
    config.merge(clonedConfig => {
      clonedConfig.plugins = [];
      return clonedConfig;
    });
    config.plugin('optimize-css', OptimizeCssAssetsPlugin);
  }

  config.removeLoader('js');
  config.loader('js', {
    test: /\.jsx?$/, // Accept either .js or .jsx files.
    exclude: /(node_modules|bower_components|client_js)/,
    loader: 'babel',
    query: {
      plugins: ['add-module-exports', 'transform-object-assign'],
      presets: ['react', 'es2015', 'stage-0']
    }
  });

  // Bundle up the client js which is just old-school jquery based javascript
  // concactenated together in a single file. Not using any sort of module loading
  // system.
  config.loader('client-js', {
    test: [/bower_components\/.*\.js$/, /client_js\/.*\.js$/],
    ignore: /node_modules/,
    loader: ExtractTextPlugin.extract(process.env.NODE_ENV === 'production' ?
      'raw!uglify' : 'raw')
  });

  config.plugin('client-js', ExtractTextPlugin, ['js/client.js']);

  return config;
};
